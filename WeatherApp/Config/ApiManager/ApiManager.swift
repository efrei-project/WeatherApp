//
//  ApiManager.swift
//  WeatherApp
//
//  Created by Louis ADAM on 22/05/2019.
//  Copyright © 2019 Louis ADAM. All rights reserved.
//

import Foundation
import Alamofire

class ApiManager {
       
    static func getDataJson(uri: String, success: @escaping (Data)->(), failure: @escaping (Error)->()) {
        let urlString = uri.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        Alamofire.request(
            urlString ?? "",
            method: .get
            )
            .responseData(completionHandler: {
                (dataResponse) in switch dataResponse.result {
                case .success(let value):
                    success(value)
                    break
                case .failure(let error):
                    failure(error)
                    break
                }
            })
    }
}
