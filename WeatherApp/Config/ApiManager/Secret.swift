//
//  Secret.swift
//  WeatherApp
//
//  Created by Louis ADAM on 22/05/2019.
//  Copyright © 2019 Louis ADAM. All rights reserved.
//

import Foundation

// supply api_key and transform variable to url
class Secret{
    var API_KEY_WEATHER: String = "43365c0cb0a3d2258333ab6bf0674043"
    var API_KEY_CITIES: String = "3a38c23d8f694324b317b29c3b51feea"
    var lattitude: String
    var longitude: String
    var unit: String = "si"
    var lang: String = "fr"
    
    init(lattitude: String, longitude: String){
        self.lattitude = lattitude
        self.longitude = longitude
    }
    
    func getUrlWeather() -> String{
        return "https://api.darksky.net/forecast/\(self.API_KEY_WEATHER)/\(self.lattitude),\(self.longitude)?units=\(self.unit)&lang=\(self.lang)"
    }
    
    func getUrlCities(name: String) -> String{
        return "https://api.opencagedata.com/geocode/v1/json?key=\(self.API_KEY_CITIES)&q=\(name)"
    }
    
    func setUnit(value: String){
        self.unit = value
    }
    
    func setLang(value: String){
        self.lang = value
    }
    
}
