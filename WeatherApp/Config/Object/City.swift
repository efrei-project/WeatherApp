//
//  City.swift
//  WeatherApp
//
//  Created by Louis ADAM on 22/05/2019.
//  Copyright © 2019 Louis ADAM. All rights reserved.
//

import Foundation
import CoreLocation

// create for read exam's subject
class City{
    var name: String
    var coordinates: CLLocationCoordinate2D
    
    init(name: String, coordinates : CLLocationCoordinate2D){
        self.name = name
        self.coordinates = coordinates
    }
}
