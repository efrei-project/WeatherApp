//
//  ChildViewControllerDelegate.swift
//  WeatherApp
//
//  Created by Louis ADAM on 24/05/2019.
//  Copyright © 2019 Louis ADAM. All rights reserved.
//

import Foundation
protocol ChildViewControllerDelegate
{
    func childViewControllerResponse(parameter: [String])
    func addCityViewControllerDelegate(city: City)
}
