//
//  Extension.swift
//  WeatherApp
//
//  Created by Louis ADAM on 24/05/2019.
//  Copyright © 2019 Louis ADAM. All rights reserved.
//

import Foundation
import CoreLocation

// Extension for calculate center of coordinates
extension CLLocationCoordinate2D {
    static func middleLocationWith(coordinate: CLLocationCoordinate2D) -> CLLocationCoordinate2D {
        
        let lon1 = coordinate.longitude * Double.pi / 180
        let lon2 = coordinate.longitude * Double.pi / 180
        let lat1 = coordinate.latitude * Double.pi / 180
        let lat2 = coordinate.latitude * Double.pi / 180
        let dLon = lon2 - lon1
        let x = cos(lat2) * cos(dLon)
        let y = cos(lat2) * sin(dLon)
        
        let lat3 = atan2( sin(lat1) + sin(lat2), sqrt((cos(lat1) + x) * (cos(lat1) + x) + y * y) )
        let lon3 = lon1 + atan2(y, cos(lat1) + x)
        
        let center:CLLocationCoordinate2D = CLLocationCoordinate2DMake(lat3 * 180 / Double.pi, lon3 * 180 / Double.pi)
        return center
    }
}
