//
//  CitiesSearch.swift
//  WeatherApp
//
//  Created by Louis ADAM on 23/05/2019.
//  Copyright © 2019 Louis ADAM. All rights reserved.
//

import Foundation

// parse the API's return
struct CitiesSearchData: Decodable {
    
    enum CodingKeys: String, CodingKey {
        case results
    }
    
    var results: [CitySearchData]
    
}
