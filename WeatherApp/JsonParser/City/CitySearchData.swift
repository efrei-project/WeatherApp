//
//  CitySearchData.swift
//  WeatherApp
//
//  Created by Louis ADAM on 23/05/2019.
//  Copyright © 2019 Louis ADAM. All rights reserved.
//

import Foundation

// parse the API's return
struct CitySearchData: Decodable {
    
    enum CodingKeys: String, CodingKey {
        case annotations
        case geometry
        case formatted
        case components
    }
    
    var annotations: AnnotationsData
    var geometry: GeometryData
    var formatted: String
    var components: ComponentsData
}
