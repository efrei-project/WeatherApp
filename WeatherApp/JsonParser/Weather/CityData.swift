//
//  CityData.swift
//  WeatherApp
//
//  Created by Louis ADAM on 22/05/2019.
//  Copyright © 2019 Louis ADAM. All rights reserved.
//

import Foundation

// parse the API's return
struct CityData: Decodable {
    
    enum CodingKeys: String, CodingKey {
        case currently
        case hourly
        case daily
    }
    
    var currently: WeatherData
    var hourly: HourlyData
    var daily: DailyData
}
