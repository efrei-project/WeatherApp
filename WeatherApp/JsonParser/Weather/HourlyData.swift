//
//  HourlyData.swift
//  WeatherApp
//
//  Created by Louis ADAM on 22/05/2019.
//  Copyright © 2019 Louis ADAM. All rights reserved.
//

import Foundation

//Hour's section of API's return
struct HourlyData: Decodable {
    
    enum CodingKeys: String, CodingKey {
        case icon
        case summary
        case data
    }
    
    var icon: String
    var summary: String
    var data: [WeatherData]
}
