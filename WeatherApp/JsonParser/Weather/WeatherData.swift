//
//  WeatherData.swift
//  WeatherApp
//
//  Created by Louis ADAM on 22/05/2019.
//  Copyright © 2019 Louis ADAM. All rights reserved.
//

import Foundation

// the pattern of each API's return
struct WeatherData: Decodable {
    
    enum CodingKeys: String, CodingKey {
        case time
        case icon
        case temperature
        case temperatureMin
        case temperatureMax
        case summary
        case windSpeed
        case pressure
        case humidity
        case uvIndex
    }
    
    var time: Double
    var icon: String
    var temperature: Double?
    var temperatureMin: Double?
    var temperatureMax: Double?
    var summary: String
    var windSpeed: Double
    var pressure: Double
    var humidity: Double
    var uvIndex: Double
}
