//
//  DailyDetailsTableViewCell.swift
//  WeatherApp
//
//  Created by Louis ADAM on 22/05/2019.
//  Copyright © 2019 Louis ADAM. All rights reserved.
//

import UIKit

class DailyDetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var day: UILabel!
    @IBOutlet weak var tempMin: UILabel!
    @IBOutlet weak var tempMax: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // set data to cell
    func setData(data: WeatherData, unit: String, lang: String){
        self.tempMax.text = "\(Int(round(data.temperatureMax ?? 0)))\(unit == "si" ? "°C" : "°F")"
        self.tempMin.text = "\(Int(round(data.temperatureMin ?? 0)))\(unit == "si" ? "°C" : "°F")"
        self.icon.image = UIImage(named: "\(data.icon).png")
        let date = Date(timeIntervalSince1970: data.time)
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: lang == "fr" ? "FR-fr" : "EN-en")
        formatter.dateFormat = "EEEE"
        self.day.text = formatter.string(from: date)
    }
}
