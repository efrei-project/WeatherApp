//
//  ExtraInfoTableViewCell.swift
//  WeatherApp
//
//  Created by Louis ADAM on 22/05/2019.
//  Copyright © 2019 Louis ADAM. All rights reserved.
//

import UIKit

class ExtraInfoTableViewCell: UITableViewCell {

    @IBOutlet weak var labelRight: UILabel!
    @IBOutlet weak var labelLeft: UILabel!
    @IBOutlet weak var valueLeft: UILabel!
    @IBOutlet weak var valueRight: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // set data to cell
    func setData(right: String, left: String, valueLeft: String, valueRight: String, unit: String){
        self.labelLeft.text = left
        self.labelRight.text = right
        self.valueLeft.text = left == "Humidity" ? "\(valueLeft)%" : "\(valueLeft) hPa"
        self.valueRight.text = right == "Wind Speed" ? "\(valueRight) \(unit == "si" ? "km/h" : "m/s")" : "\(valueRight)"
    }
}
