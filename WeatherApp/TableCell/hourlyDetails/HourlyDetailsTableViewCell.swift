//
//  HourlyDetailsTableViewCell.swift
//  WeatherApp
//
//  Created by Louis ADAM on 22/05/2019.
//  Copyright © 2019 Louis ADAM. All rights reserved.
//

import UIKit

class HourlyDetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var hour: UILabel!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var humidity: UILabel!
    @IBOutlet weak var temperature: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // set data to cell
    func setData(data: WeatherData, unit: String){
        self.temperature.text = "\(Int(round(data.temperature ?? 0)))\(unit == "si" ? "°C" : "°F")"
        self.icon.image = UIImage(named: "\(data.icon).png")
        self.humidity.text = data.humidity > 0 ? "\(data.humidity)%" : ""
        
        let date = Date(timeIntervalSince1970: data.time)
        let formatter = DateFormatter()
        formatter.dateFormat = "HH"
        
        self.hour.text = formatter.string(from: date)
        
    }
}
