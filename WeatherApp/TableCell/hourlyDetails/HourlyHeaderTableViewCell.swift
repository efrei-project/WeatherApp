//
//  HourlyHeaderTableViewCell.swift
//  WeatherApp
//
//  Created by Louis ADAM on 22/05/2019.
//  Copyright © 2019 Louis ADAM. All rights reserved.
//

import UIKit

class HourlyHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var data: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // set data to cell
    func setData(data: HourlyData){
        self.data.text = data.summary
    }
    
}
