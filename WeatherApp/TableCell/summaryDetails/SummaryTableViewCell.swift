//
//  SummaryTableViewCell.swift
//  WeatherApp
//
//  Created by Louis ADAM on 22/05/2019.
//  Copyright © 2019 Louis ADAM. All rights reserved.
//

import UIKit
import SDWebImage

class SummaryTableViewCell: UITableViewCell {

    @IBOutlet weak var backgroundImg: UIImageView!
    @IBOutlet weak var summary: UILabel!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var temperature: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // set data to cell
    func setData(data: WeatherData, unit: String){
        self.icon.image = UIImage(named: "\(data.icon).png")
        var image: String
        switch data.icon {
        case "snow", "sleet" :
            image = "snowBack.jpg"
            self.setColor(font: UIColor.white, border: UIColor.black, summary: data.summary, temperature: "\(Int(round(data.temperature ?? 0)))\(unit == "si" ? "°C" : "°F")")
        case "clear-day", "partly-cloudy-day":
            image = "skyBack.jpg"
            self.setColor(font: UIColor.black, border: UIColor.white, summary: data.summary, temperature: "\(Int(round(data.temperature ?? 0)))\(unit == "si" ? "°C" : "°F")")
        case "wind", "fog", "cloudy", "hail", "thunderstorm", "tornado":
            image = "cloudBack.jpg"
            self.setColor(font: UIColor.white, border: UIColor.black, summary: data.summary, temperature: "\(Int(round(data.temperature ?? 0)))\(unit == "si" ? "°C" : "°F")")
        case "clear-night", "partly-cloudy-night":
            image = "nightBack.jpg"
            self.setColor(font: UIColor.white, border: UIColor.black, summary: data.summary, temperature: "\(Int(round(data.temperature ?? 0)))\(unit == "si" ? "°C" : "°F")")
        case "rain":
            image = "rainBack.png"
            self.setColor(font: UIColor.black, border: UIColor.white, summary: data.summary, temperature: "\(Int(round(data.temperature ?? 0)))\(unit == "si" ? "°C" : "°F")")
        default:
            image = "skyBack.jpg"
            self.setColor(font: UIColor.white, border: UIColor.white, summary: data.summary, temperature: "\(Int(round(data.temperature ?? 0)))\(unit == "si" ? "°C" : "°F")")
        }
        self.backgroundImg.image = UIImage(named: image)
    }
    
    // set color and outline
    func setColor(font: UIColor, border: UIColor, summary: String, temperature: String){
        
        let sum = NSAttributedString(string: summary, attributes: [
            NSAttributedString.Key.foregroundColor : font,
            NSAttributedString.Key.strokeColor : border,
            NSAttributedString.Key.strokeWidth : -1,
            NSAttributedString.Key.font : UIFont.systemFont(ofSize: 17)
            ])
        self.summary.attributedText = sum
        self.summary.textColor = UIColor.black
        let temp = NSAttributedString(string: temperature, attributes: [
            NSAttributedString.Key.foregroundColor : font,
            NSAttributedString.Key.strokeColor : border,
            NSAttributedString.Key.strokeWidth : -1,
            NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12)
            ])
        self.temperature.attributedText = temp
        
    }
    
}

