//
//  AddCityViewController.swift
//  WeatherApp
//
//  Created by Louis ADAM on 23/05/2019.
//  Copyright © 2019 Louis ADAM. All rights reserved.
//

import UIKit
import CoreLocation

class AddCityViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate{

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var searchField: UISearchBar!
    
    @IBOutlet weak var tableView: UITableView!
    
    var citiesSearchData: CitiesSearchData?
    var secret: Secret?
    var delegate: ChildViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        searchField.delegate = self
        self.secret = Secret(lattitude: "", longitude: "")
        tableView.register(UINib(nibName: "SearchCitiesTableViewCell", bundle: nil), forCellReuseIdentifier: "ListCities")
    }
    
    // func when change in searchBar - disable cause of lot of request
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
//        ApiManager.getDataJson(uri: self.secret?.getUrlCities(name: searchBar.text ?? "") ?? "", success: { (datas) in
//            let decoder = JSONDecoder()
//            self.citiesSearchData = (
//                try? decoder.decode(CitiesSearchData.self, from: datas)
//            )
//            self.tableView.reloadData()
//        }, failure: { (error) in
//            print(error)
//        })
    }
    
    // func when enter
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        ApiManager.getDataJson(uri: self.secret?.getUrlCities(name: searchBar.text ?? "") ?? "", success: { (datas) in
            let decoder = JSONDecoder()
            self.citiesSearchData = (
                try? decoder.decode(CitiesSearchData.self, from: datas)
            )
            self.tableView.reloadData()
        }, failure: { (error) in
            print(error)
        })
    }
    
    // number of row
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.citiesSearchData?.results.count ?? 1
    }
    
    // set data in each row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let _city = citiesSearchData?.results{
            let _cell = tableView.dequeueReusableCell(withIdentifier: "ListCities", for: indexPath) as! SearchCitiesTableViewCell
            _cell.setData(name: _city[indexPath.row].formatted)
            return _cell
        }
        return UITableViewCell()
    }
    
    //Function to save city and navigate from thisList to detail
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let coordinate = CLLocationCoordinate2D(latitude: citiesSearchData?.results[indexPath.row].geometry.lat ?? 0, longitude: citiesSearchData?.results[indexPath.row].geometry.lng ?? 0)
        let _city = City(name: citiesSearchData?.results[indexPath.row].components.city ?? "", coordinates: coordinate)
        self.delegate?.addCityViewControllerDelegate(city: _city)
        self.navigationController?.popViewController(animated:true)
    }

  
}


