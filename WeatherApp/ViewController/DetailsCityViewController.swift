//
//  DetailsCityViewController.swift
//  WeatherApp
//
//  Created by Louis ADAM on 22/05/2019.
//  Copyright © 2019 Louis ADAM. All rights reserved.
//

import UIKit

class DetailsCityViewController: UIViewController, UITableViewDelegate, UITableViewDataSource  {
    

    @IBOutlet weak var tableView: UITableView!
    var city: City?
    var secret: Secret?
    var cityData: CityData?
    var params: [String]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set title
        self.title = "Weather's App - \(self.city?.name ?? "")"
        
        // prepare Tableview
        tableView.delegate = self
        tableView.dataSource = self
        self.secret = Secret(lattitude: self.city?.coordinates.latitude.description ?? "", longitude: self.city?.coordinates.longitude.description ?? "")
        self.secret?.setLang(value: params?[0] ?? "fr")
        self.secret?.setUnit(value: params?[1] ?? "si")
        
        // Request
        ApiManager.getDataJson(uri: self.secret?.getUrlWeather() ?? "", success: { (datas) in
            let decoder = JSONDecoder()
            self.cityData = (
                try? decoder.decode(CityData.self, from: datas)
            )
            self.cityData!.hourly.data = [WeatherData](self.cityData!.hourly.data.prefix(24))
            
            self.tableView.reloadData()
        }, failure: { (error) in
            print(error)
        })
        
        // Regsiter cell
        tableView.register(UINib(nibName: "SummaryTableViewCell", bundle: nil), forCellReuseIdentifier: "Summary")
        tableView.register(UINib(nibName: "HourlyHeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "HourlyHeader")
        tableView.register(UINib(nibName: "HourlyDetailsTableViewCell", bundle: nil), forCellReuseIdentifier: "HourlyDetails")
        tableView.register(UINib(nibName: "DailyHeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "DailyHeader")
        tableView.register(UINib(nibName: "DailyDetailsTableViewCell", bundle: nil), forCellReuseIdentifier: "DailyDetails")
        tableView.register(UINib(nibName: "ExtraInfoTableViewCell", bundle: nil), forCellReuseIdentifier: "ExtraInfo")
        tableView.register(UINib(nibName: "ExtraInfoHeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "ExtraInfoHeader")
    }

    
    // set number of section
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    // set number of row by section
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return cityData?.hourly.data.count ?? 0
        case 2:
            return cityData?.daily.data.count ?? 0
        case 3:
            return 3
        default:
            return 1
        }
    }
    
    // set data in cell / by row / by section
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            if let _city = cityData{
                let _cell = tableView.dequeueReusableCell(withIdentifier: "Summary", for: indexPath) as! SummaryTableViewCell
                _cell.setData(data: _city.currently, unit: self.params?[1] ?? "si")
                return _cell
            }
        case 1:
            if indexPath.row == 0{
                if let _city = cityData{
                    let _cell = tableView.dequeueReusableCell(withIdentifier: "HourlyHeader", for: indexPath) as! HourlyHeaderTableViewCell
                    _cell.setData(data: _city.hourly)
                    return _cell
                }
            } else {
                if let _city = cityData{
                    let _cell = tableView.dequeueReusableCell(withIdentifier: "HourlyDetails", for: indexPath) as! HourlyDetailsTableViewCell
                    _cell.setData(data: _city.hourly.data[indexPath.row], unit: self.params?[1] ?? "si")
                    return _cell
                }
            }
        case 2:
            if indexPath.row == 0{
                if let _city = cityData{
                    let _cell = tableView.dequeueReusableCell(withIdentifier: "DailyHeader", for: indexPath) as! DailyHeaderTableViewCell
                    _cell.setData(data: _city.daily)
                    return _cell
                }
            } else {
                if let _city = cityData{
                    let _cell = tableView.dequeueReusableCell(withIdentifier: "DailyDetails", for: indexPath) as! DailyDetailsTableViewCell
                    _cell.setData(data: _city.daily.data[indexPath.row], unit: self.params?[1] ?? "si", lang: self.params?[0] ?? "fr")
                    return _cell
                }
            }
        case 3:
            if indexPath.row == 0{
                if let _city = cityData{
                    let _cell = tableView.dequeueReusableCell(withIdentifier: "ExtraInfoHeader", for: indexPath) as! ExtraInfoHeaderTableViewCell
                    _cell.setData(text: self.params?[0] == "fr" ? "Informations supplémentaires" : "Extra Informations")
                    return _cell
                }
            } else if indexPath.row == 1{
                if let _city = cityData{
                    let _cell = tableView.dequeueReusableCell(withIdentifier: "ExtraInfo", for: indexPath) as! ExtraInfoTableViewCell
                    _cell.setData(right: self.params?[0] == "fr" ? "Vitesse du vent" : "Wind Speed", left: self.params?[0] == "fr" ? "Humidité" : "Humidity", valueLeft: "\(_city.currently.humidity)", valueRight: "\(_city.currently.windSpeed)", unit: self.params?[1] ?? "si")
                    return _cell
                }
            } else {
                if let _city = cityData{
                    let _cell = tableView.dequeueReusableCell(withIdentifier: "ExtraInfo", for: indexPath) as! ExtraInfoTableViewCell
                    _cell.setData(right: self.params?[0] == "fr" ? "Indice d'UV" : "UV Index", left: self.params?[0] == "fr" ? "Pression atmosphérique" : "Pressure", valueLeft: "\(_city.currently.pressure)", valueRight: "\(_city.currently.uvIndex)", unit: self.params?[1] ?? "si")
                    return _cell
                }
            }
            
        default:
            return UITableViewCell()
        }
        return UITableViewCell()
    }
    
}
