//
//  MapViewController.swift
//  WeatherApp
//
//  Created by Louis ADAM on 22/05/2019.
//  Copyright © 2019 Louis ADAM. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, MKMapViewDelegate, ChildViewControllerDelegate {
    
    // Declare variable
    @IBOutlet weak var addButton: UIBarButtonItem!
    @IBOutlet weak var settings: UIBarButtonItem!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var choiceSelector: UISegmentedControl!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var navBar: UINavigationBar!
    var cities: [City] = CitiesData.list
    var settingsParam: [String] = ["fr", "si"]
    
    // onLoad func
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "ListCitiesTableViewCell", bundle: nil), forCellReuseIdentifier: "ListCities")
        mapView.delegate = self
        self.title = "Weather's App"
        self.setAnnot()
        self.addButton.action = #selector(switchAddCity(sender:))
        self.tableView.isHidden = true
        settings.action = #selector(buttonClicked(sender:))
        
    }
    
    //function to set Pin
    func setAnnot(){
        self.mapView.removeAnnotations(self.mapView.annotations)
        for city in cities{
            let pin = MKPointAnnotation()
            pin.title = city.name
            pin.coordinate = city.coordinates
            self.mapView.addAnnotation(pin)
        }
        self.mapView.reloadInputViews()
    }
    
    // button to navigate from thisView to AddCityView
    @objc func switchAddCity(sender: UIBarButtonItem) {
        let storyBoard = UIStoryboard(name: "Main", bundle:nil)
        let AddCityViewController = storyBoard.instantiateViewController(withIdentifier: "AddCityViewController") as! AddCityViewController
        AddCityViewController.delegate = self
        self.navigationController?.pushViewController(AddCityViewController, animated: true)
    }
    
    // Button navigate thisView to settingsView
    @objc func buttonClicked(sender: UIBarButtonItem) {
        let goNext = storyboard?.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
        goNext.delegate = self
        goNext.params = self.settingsParam
        self.navigationController?.pushViewController(goNext, animated: true)
    }
    
    
    var selectedAnnotation: MKAnnotation?
    
    // navigate from Map to Details
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if let _city = view.annotation?.title, let _coords = view.annotation?.coordinate {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "DetailsCityViewController") as? DetailsCityViewController
            vc?.city = City(name: _city ?? "Unknown city name", coordinates: _coords)
            vc?.params = self.settingsParam
            let middleLocation = CLLocationCoordinate2D.middleLocationWith(coordinate: vc?.city?.coordinates ?? CLLocationCoordinate2D(latitude: 0, longitude: 0))
            self.mapView.setRegion(MKCoordinateRegion(center: middleLocation, span: MKCoordinateSpan(latitudeDelta: 10, longitudeDelta: 10)), animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute: {
                self.navigationController?.pushViewController(vc!, animated: true)
            })
            
        }
    }
    
    // change view between Map and List
    @IBAction func segmentedControlAction(sender: UISegmentedControl) {
        switch choiceSelector.selectedSegmentIndex {
        case 0:
            self.tableView.isHidden = true
            self.mapView.isHidden = false
        case 1:
            self.tableView.isHidden = false
            self.mapView.isHidden = true
        default:
            print("problem")
        }
    }
    
    // delegate to get the settings
    func childViewControllerResponse(parameter: [String])
    {
        self.settingsParam = parameter
    }
    
    // delegate to get the city
    func addCityViewControllerDelegate(city: City) {
        self.cities.append(city)
        self.tableView.reloadData()
        self.setAnnot()
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DetailsCityViewController") as? DetailsCityViewController
        vc?.city = city
        vc?.params = self.settingsParam
        let middleLocation = CLLocationCoordinate2D.middleLocationWith(coordinate: vc?.city?.coordinates ?? CLLocationCoordinate2D(latitude: 0, longitude: 0))
        self.mapView.setRegion(MKCoordinateRegion(center: middleLocation, span: MKCoordinateSpan(latitudeDelta: 0.9, longitudeDelta: 0.9)), animated: true)
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute: {
            self.navigationController?.pushViewController(vc!, animated: true)
        })
    }
    
    
    // -------------------------------
    // ----------List Cities ---------
    // -------------------------------
    
    
    // number of row
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cities.count
    }
    
    // set cellController
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let _cell = tableView.dequeueReusableCell(withIdentifier: "ListCities", for: indexPath) as! ListCitiesTableViewCell
        _cell.setData(name: cities[indexPath.row].name)
        return _cell
    }
    
    //Function to navigate from list to detail
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let _cities = cities[indexPath.row]
        let storyBoard = UIStoryboard(name: "Main", bundle:nil)
        let cityViewController = storyBoard.instantiateViewController(withIdentifier: "DetailsCityViewController") as! DetailsCityViewController
        cityViewController.city = City(name: _cities.name, coordinates: _cities.coordinates)
        cityViewController.params = self.settingsParam
        self.navigationController?.pushViewController(cityViewController, animated:true)
    }
}


