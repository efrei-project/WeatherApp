//
//  SettingsViewController.swift
//  WeatherApp
//
//  Created by Louis ADAM on 23/05/2019.
//  Copyright © 2019 Louis ADAM. All rights reserved.
//

import UIKit


class SettingsViewController: UIViewController {

    @IBOutlet weak var unitButton: UISegmentedControl!
    @IBOutlet weak var langButton: UISegmentedControl!
    @IBOutlet weak var unitLabel: UILabel!
    @IBOutlet weak var langLabel: UILabel!
    @IBOutlet weak var doneButton: UIButton!
    
    var params: [String]?
    var oldParams: [String]?
    var delegate: ChildViewControllerDelegate?
    
    // onLoad function
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Weather's App - Parametres"
        self.oldParams = self.params ?? ["", ""]
        self.langButton.selectedSegmentIndex = self.params?[0] == "fr" ? 0 : 1
        self.unitButton.selectedSegmentIndex = self.params?[1] == "si" ? 0 : 1
    }
    
    // listener changeLang
    @IBAction func changLang(sender: UISegmentedControl) {
        switch langButton.selectedSegmentIndex {
            case 0:
                self.params?[0] = "fr"
                self.langLabel.text = "Langage"
                self.unitLabel.text = "Unités"
                self.title = "Weather's App - Parametres"
                self.doneButton.setTitle("Sauvegarder", for: .normal)
            case 1:
                self.params?[0] = "en"
                self.langLabel.text = "Language"
                self.unitLabel.text = "Units"
                self.title = "Weather's App - Settings"
                self.doneButton.setTitle("Save", for: .normal)
            default:
                print("problem")
        }
    }
    
    // listener changeUnit
    @IBAction func changeUnit(sender: UISegmentedControl){
        switch unitButton.selectedSegmentIndex {
            case 0:
                self.params?[1] = "si"
            case 1:
                self.params?[1] = "us"
            default:
                print("problem")
        }
    }
    
    // Listener saveSettings
    @IBAction func doneClicked(_ sender: Any) {
        self.delegate?.childViewControllerResponse(parameter: self.params ?? ["", ""])
        self.navigationController?.popViewController(animated:true)
    }
    
    
    
}
